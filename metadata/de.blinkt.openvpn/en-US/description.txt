Openvpn for Android is an open source client based on the open source OpenVPN project.
It uses the VPNService API of Android 4.0+ and requires neither Jailbreak nor root on your telephone.

FAQ

Can I get free Internet
No, this app is for connecting to an OpenVPN server.

How to connect
OpenVPN is a client software to connect to an OpenVPN server. It is not an APP selling or provding any VPN services.
It allows to your own/company/university/provider OpenVPN server or to the VPN service of many of the commercial
VPN providers.

What is the difference between all the OpenVPN apps?
For more information about the different OpenVPN clients in the Playstore see this: http://ics-openvpn.blinkt.de/FAQ.html#faq_androids_clients_title

Access to your photos/media (Android older than 6.0)
This app implements a feature to import OpenVPN profiles from the SDCard/internal memory. Google categorizes this access "accessing your media and photos"

TAP Mode
Only tun mode support (Sorry no tap, with Android 4.0 only tun can be supported).

Joining Beta
Prerelease versions available by joining G+ group: https://plus.google.com/communities/114121831091105660092

Translate the app
If you want to help to translate OpenVPN into your native language look at the homepage of this project.

Bug reports
Please report bug/suggestions via email or at the code Google Code project. But please read the FAQ before writing me.

Security
OpenSSL Heartbleed: OpenVPN for Android uses its own non vulnerable OpenSSL version. For more details about OpenVPN and Heartbleed see: https://community.openvpn.net/openvpn/wiki/heartbleed
 
